﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LoadWalker
{
    class Program
    {
        private static Uri uri;
        private static int maxQps;
        private static int duration;

        private static long maxResponse = -1;
        private static long minResponse = -1;
        private static int qps = 0;

        private static ReaderWriterLockSlim statsLock = new ReaderWriterLockSlim();
        private static ReaderWriterLockSlim qpsLock = new ReaderWriterLockSlim();

        private static bool shouldExit = false;

        static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                Usage();
                return;
            }

            try
            {
                uri = new Uri(args[0]); // test valid uri
                maxQps = int.Parse(args[1]);
                duration = int.Parse(args[2]);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Incorrect parameters: {0}", ex.Message));
                return;
            }

            StartTests();
        }

        private static void Usage()
        {
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("loadwalker <url> <max qps> <duration>");
            Console.WriteLine("Loadwalker will invoke up to <max qps> requests per seconds to <url> and last for <duration> seconds.");
            Console.WriteLine("Currently loadwalker only supports HTTP GET.");
            Console.WriteLine("eg: loadwalker http://localhost/ 1000 60");
        }

        private static void StartTests()
        {
            ServicePointManager.DefaultConnectionLimit = 1024;  // increase global default per-host connection limit on the client side

            var sw = Stopwatch.StartNew();
            Console.WriteLine(string.Format("Target {0} Max QPS {1} Duration {2} seconds", uri, maxQps, duration));
            Console.WriteLine("Start test on {0}", DateTime.Now);
            Console.WriteLine("Realtime stats:");
            Console.WriteLine(string.Format("QPS: {0} | Min: {1} ms | Max: {2} ms", qps, minResponse, maxResponse));

            // create all threads and run
            var threads = new Thread[maxQps];
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(ThreadExecute);
                threads[i].IsBackground = true;
            }

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Start();
            }

            // Here we collect stats every 1000ms.  We could do faster if we want more accurate stats refresh
            while (true)
            {
                if (sw.ElapsedMilliseconds >= duration * 1000)
                {
                    shouldExit = true;
                    break;
                }

                Thread.Sleep(1000);

                // thread safe collect stats
                int tempQps = 0;
                long tempMin = -1, tempMax = -1;
                qpsLock.EnterWriteLock();
                try
                {
                    tempQps = qps;  // current qps
                    qps = 0;    // reset qps for next 1000ms
                }
                finally
                {
                    qpsLock.ExitWriteLock();
                }

                statsLock.EnterReadLock();
                try
                {
                    tempMin = minResponse;
                    tempMax = maxResponse;
                }
                finally
                {
                    statsLock.ExitReadLock();
                }

                // IO max/min/qps
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                Console.WriteLine(string.Format("QPS: {0} | Min: {1} ms | Max: {2} ms            ", tempQps, tempMin, tempMax));
            }

            sw.Stop();
            Console.WriteLine("Test completed");
        }

        private static void ThreadExecute()
        {
            // main thread body.  As long as current qps < maxQps we will create a new HTTP request
            while (!shouldExit)
            {
                qpsLock.EnterUpgradeableReadLock();
                try
                {
                    if (qps < maxQps)
                    {
                        qpsLock.EnterWriteLock();
                        try
                        {
                            qps++;
                            HttpGetJob();
                        }
                        finally
                        {
                            qpsLock.ExitWriteLock();
                        }
                    }
                    else
                    {
                        Thread.Sleep(10);
                    }
                }
                finally
                {
                    qpsLock.ExitUpgradeableReadLock();
                }
            }
        }

        private static void HttpGetJob()
        {
            DateTime start = DateTime.Now;
            var sw = Stopwatch.StartNew();

            try
            {
                var request = HttpWebRequest.Create(uri);
                request.GetResponseAsync().ContinueWith(task =>
                {
                    ReportJobResult(start, sw.ElapsedMilliseconds, task.Result);
                    sw.Stop();
                });
            }
            catch (Exception ex)
            {
                //TODO: log to file or reports
            }

            // report result

        }

        private static void ReportJobResult(DateTime start, long responseTime, WebResponse response)
        {
            statsLock.EnterUpgradeableReadLock();
            try
            {
                if (minResponse == -1 || maxResponse == -1
                    || responseTime < minResponse || responseTime > maxResponse)
                {
                    // upgrade to write lock
                    statsLock.EnterWriteLock();
                    try
                    {
                        if (minResponse == -1)
                            minResponse = responseTime;
                        if (maxResponse == -1)
                            maxResponse = responseTime;
                        if (responseTime < minResponse)
                            minResponse = responseTime;
                        if (responseTime > maxResponse)
                            maxResponse = responseTime;
                    }
                    finally
                    {
                        statsLock.ExitWriteLock();
                    }
                }
            }
            finally
            {
                statsLock.ExitUpgradeableReadLock();
            }

            //TODO: log time and response code to a file
        }
    }
}
